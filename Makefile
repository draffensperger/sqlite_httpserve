# Makefile for sqlite_httpserve for GNU Make
# See README.md for build requirements, instructions and options
#
# Copyright (c) 2013 David A. Raffensperger (contact at davidraff dot com)
#
# Distributed under the Boost Software License, Version 1.0. (See accompanying
# file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
#

# To compile in debug mode run "make MODE=debug"
MODE=release

# Show all warnings and generate dependency graph
CFLAGS=-c -Wall -MMD

# On Windows the extension .exe will be appended
EXECUTABLE=sqlite_httpserve

# Get the system name info for platform specific variables
UNAME := $(shell uname)
UNAME_OS := $(shell uname -o)

# MinGW on Windows
ifeq ($(UNAME_OS), Msys)
# -------------------------
BOOST_ROOT=C:/Program Files/boost/boost_1_55_0
ifeq ($(MODE),debug)
BOOST_LIB_SUFFIX=-mgw48-mt-d-1_55
else
BOOST_LIB_SUFFIX=-mgw48-mt-1_55
endif
LDFLAGS = -LC:/MinGW/lib -L"$(BOOST_ROOT)/stage/lib"
LIBS = -Wl,-Bstatic -lboost_system$(BOOST_LIB_SUFFIX) -lwsock32 -lws2_32
CFLAGS += -isystem "$(BOOST_ROOT)" -D_WIN32_WINNT=0x0501


# Mac OS X
else ifeq ($UNAME, Darwin)
# -------------------------


# Linux and Unix-based systems
else
# -------------------------
LDFLAGS =
LIBS = -Wl,-Bstatic -lboost_system -Wl,-Bdynamic -lpthread -ldl
CFLAGS += -pthread
endif

# Set compiler flags for debug/release mode
ifeq ($(MODE),debug)
CFLAGS += -g -DDEBUG
else
CFLAGS += -O2 -DNDEBUG
endif

# This makefile searches for all sources ending in .c and .cpp in the src/ folder and subfolders
# find adapted from: https://plus.google.com/+MadsAalundNielsen/posts/h5Xr1i8kgfu
find = $(foreach dir_or_file, $(wildcard $(1)/*), $(dir_or_file) $(call find, $(dir_or_file)))
SOURCES=$(filter %.c %.cpp, $(call find, src)﻿)

# Will compile source files into .o files in obj/[release|debug]/
OBJ_CPP_ONLY=$(SOURCES:src/%.cpp=obj/$(MODE)/%.o)
OBJ_CPP_AND_C=$(OBJ_CPP_ONLY:src/%.c=obj/$(MODE)/%.o)
OBJECTS=$(OBJ_CPP_AND_C)

# Default target builds the executable in bin/[release|debug]/
executable: $(SOURCES) bin/$(MODE)/$(EXECUTABLE)

# Link together object files
bin/$(MODE)/$(EXECUTABLE): $(OBJECTS)
	mkdir -p $(@D)
	g++ -o $@ $(LDFLAGS) $(OBJECTS) $(LIBS)

# Compile .cpp files
obj/$(MODE)/%.o: src/%.cpp
	mkdir -p $(@D)
	g++ $(CFLAGS) -c -o $@ $<

# Compile .c files
obj/$(MODE)/%.o: src/%.c
	mkdir -p $(@D)
	gcc $(CFLAGS) $< -o $@

# Remove objects and executables
clean:
	rm -rf obj
	rm -rf bin

.PHONY: clean executable

# Include automatic dependency graph files from gcc -MMD
-include $(OBJFILES:.o=.d)
