// Taken from: http://www.dreamincode.net/forums/topic/122300-sqlite-in-c/

#include "sqlite_database.hpp"
#include <iostream>

sqlite_database::sqlite_database(const string& filename)
{
	sqlite_db_ = NULL;
	open(filename);
}

sqlite_database::~sqlite_database()
{
}

bool sqlite_database::open(const string& filename)
{
	 if(sqlite3_open(filename.c_str(), &sqlite_db_) == SQLITE_OK)
		return true;

	return false;
}

result_set sqlite_database::query(const string& query)
{
    result_set results;
	sqlite3_stmt *statement;

	if(sqlite3_prepare_v2(sqlite_db_, query.c_str(), -1, &statement, 0) == SQLITE_OK)
	{
		int cols = sqlite3_column_count(statement);
		int result = 0;

		for (int i = 0; i < cols; i++) {
            results.fields.push_back(sqlite3_column_name(statement, i));
		}

		while(true)
		{
			result = sqlite3_step(statement);

			if(result == SQLITE_ROW)
			{
				vector<string> values;
				for(int col = 0; col < cols; col++)
				{
					values.push_back((char*)sqlite3_column_text(statement, col));
				}
				results.data.push_back(values);
			}
			else
			{
				break;
			}
		}

		sqlite3_finalize(statement);
	}

	string error = sqlite3_errmsg(sqlite_db_);
	if(error != "not an error") cout << query << " " << error << endl;

	return results;
}

void sqlite_database::close()
{
	sqlite3_close(sqlite_db_);
}

