//
// general_handler.hpp
// ~~~~~~~~~~~~~~~~
//
// Modified by David Raffensperger
//
// Taken from file_handler.hpp as part of the server4 example with boost asio
// Copyright (c) 2003-2012 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef HTTP_SERVER4_general_handler_HPP
#define HTTP_SERVER4_general_handler_HPP

#include <string>
#include "sqlite_database.hpp"

namespace http {
namespace server4 {

struct reply;
struct request;

/// The common handler for all incoming requests.
class general_handler
{
public:
  /// Construct with a directory containing files to be served.
  explicit general_handler(const std::string& doc_root, const std::string& db_file);

  /// Handle a request and produce a reply.
  void operator()(const request& req, reply& rep);

  ~general_handler();

private:
  /// The directory containing the files to be served.
  std::string doc_root_;

  /// Location of the SQLLite3 Database
  std::string db_file_;

  /// Perform URL-decoding on a string. Returns false if the encoding was
  /// invalid.
  static bool url_decode(const std::string& in, std::string& out);
};

} // namespace server4
} // namespace http

#endif // HTTP_SERVER4_general_handler_HPP
