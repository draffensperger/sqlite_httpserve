#ifndef URI_CODEC_H
#define URI_CODEC_H


// Taken from: http://www.codeguru.com/cpp/cpp/algorithms/strings/article.php/c12759/URI-Encoding-and-Decoding.htm

// Uri encode and decode.
// RFC1630, RFC1738, RFC2396

#include <string>
    
std::string UriDecode(const std::string & sSrc);

std::string UriEncode(const std::string & sSrc);

#endif