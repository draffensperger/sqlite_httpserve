//
// general_handler.cpp
// ~~~~~~~~~~~~~~~~
//
// Based on boost_asio_server4/file_handler.cpp which is
// Copyright (c) 2003-2012 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// New helper functions and changes are Copyright (c) 2013 David Raffensperger
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#include "general_handler.hpp"
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <boost/lexical_cast.hpp>
#include "uri_codec.hpp"
#include "boost_asio_server4/mime_types.hpp"
#include "boost_asio_server4/reply.hpp"
#include "boost_asio_server4/request.hpp"

using namespace std;

namespace http {
namespace server4 {

general_handler::general_handler(const string& doc_root, const string& db_file)
	: doc_root_(doc_root), db_file_(db_file)
{
}

// Taken from: http://stackoverflow.com/questions/3418231/c-replace-part-of-a-string-with-another-string
void replaceAll(string& str, const string& from, const string& to) {
    if (from == "")
    {
        return;
    }

    size_t start_pos = 0;
    while((start_pos = str.find(from, start_pos)) != string::npos) {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length(); // In case 'to' contains 'from', like replacing 'x' with 'yx'
    }
}

//////////////////////////////////////////////////////////////////////////////
// Helper functions below added by David Raffensperger
//////////////////////////////////////////////////////////////////////////////

// Encodes a string as suitable for SQL
string sql_str(const string& str) {
    string out = str;
    replaceAll(out, "'", "''");
    return "'" + out + "'";
}

string fill_template(const string& sql_template, const vector<string>& req_fields, const vector<string>& req_values) {
    string out = sql_template;

    for (size_t i = 0; i < req_fields.size(); i++) {
        replaceAll(out, "{" + req_fields.at(i) + "}", sql_str(req_values.at(i)));
    }

    return out;
}

string csv_encode(const string& s, const string& field_delim, const string& row_delim, const string& quote = "\"") {
    bool has_quote = s.find(quote) != string::npos;
    bool need_quotes = (s.find(field_delim) != string::npos ||
                        s.find(row_delim) != string::npos ||
                        has_quote);

    if (need_quotes) {
        string out = s;
        replaceAll(out, quote, quote + quote);
        return quote + out + quote;
    } else {
        return s;
    }
}

string results_as_csv(const result_set& results, bool field_headers = true) {
    string out = "";
    string field_delim = ",";
    string row_delim = "\n";

    size_t num_fields = results.fields.size();
    size_t num_data_rows = results.data.size();

    if (field_headers) {
        for (size_t i = 0; i < num_fields; i++) {
            if (i > 0) {
                out += field_delim;
            }
            out += results.fields[i];
        }
        if (num_data_rows > 0) {
            out += row_delim;
        }
    }

    if (num_fields > 0) {
        for (size_t i = 0; i < num_data_rows; i++) {
            if (i > 0) {
                out += row_delim;
            }

            vector<string> values = results.data.at(i);
            for (size_t j = 0; j < values.size(); j++) {
                if (j > 0) {
                    out += field_delim;
                }
                out += csv_encode(values.at(j), field_delim, row_delim);
            }
        }
    }

    return out;
}

//////////////////////////////////////////////////////////////////////////////
// Based on code from boost_asio_server4/file_handler.cpp, but modified by
// David Raffensperger to look for .sql files and run them as queries
// on the sqlite database.
//////////////////////////////////////////////////////////////////////////////

void general_handler::operator()(const request& req, reply& rep)
{
    string uri_path_part;
    vector<string> req_fields;
    vector<string> req_values;

    size_t question_mark_loc = req.uri.find("?");
    if (question_mark_loc != string::npos) {
        uri_path_part = req.uri.substr(0, question_mark_loc);

        string params_part = req.uri.substr(question_mark_loc + 1);

        size_t old_loc = 0;
        size_t loc = 0;
        size_t params_part_len = params_part.length();
        while (loc < params_part_len) {
            loc = params_part.find("&", old_loc);

            if (loc == string::npos) {
                loc = params_part_len;
            }

            string param = params_part.substr(old_loc, loc - old_loc);
            size_t equals_loc = param.find("=");
            if (equals_loc != string::npos) {
                string field = param.substr(0, equals_loc);
                string value = param.substr(equals_loc + 1);

				// Decode the URL formatted field and value
				field = UriDecode(field);
				value = UriDecode(value);

                req_fields.push_back(field);
                req_values.push_back(value);
            }

            old_loc = loc + 1;
        }
    } else {
        uri_path_part = req.uri;
    }

	// Decode url to path.
	string request_path;
	if (!url_decode(uri_path_part, request_path))
	{
		rep = reply::stock_reply(reply::bad_request);
		return;
	}

	// Request path must be absolute and not contain "..".
	if (request_path.empty() || request_path[0] != '/'
			|| request_path.find("..") != string::npos)
	{
		rep = reply::stock_reply(reply::bad_request);
		return;
	}

	// If path ends in slash (i.e. is a directory) then add "index.html".
	if (request_path[request_path.size() - 1] == '/')
	{
		request_path += "index.html";
	}

	// Determine the file extension.
	size_t last_slash_pos = request_path.find_last_of("/");
	size_t last_dot_pos = request_path.find_last_of(".");
	string extension;
	if (last_dot_pos != string::npos && last_dot_pos > last_slash_pos)
	{
		extension = request_path.substr(last_dot_pos + 1);
	}

    string full_path = doc_root_ + request_path;

    if (extension == "sql") {
        ifstream is(full_path.c_str(), ios::in | ios::binary);
        if (!is) {
            rep = reply::stock_reply(reply::not_found);
            return;
        }
        stringstream buffer;
        buffer << is.rdbuf();
        string sql_template = buffer.str();
        is.close();


        string sql = fill_template(sql_template, req_fields, req_values);

        sqlite_database db(db_file_);
        result_set results = db.query(sql);
        db.close();

        rep.status = reply::ok;
        rep.content = results_as_csv(results);
    } else {
        // Open the file to send back.
        ifstream is(full_path.c_str(), ios::in | ios::binary);
        if (!is)
        {
            rep = reply::stock_reply(reply::not_found);
            return;
        }

        // Fill out the reply to be sent to the client.
        rep.status = reply::ok;
        char buf[512];
        while (is.read(buf, sizeof(buf)).gcount() > 0)
            rep.content.append(buf, is.gcount());
    }

	rep.headers.resize(2);
	rep.headers[0].name = "Content-Length";
	rep.headers[0].value = boost::lexical_cast<string>(rep.content.size());
	rep.headers[1].name = "Content-Type";
	rep.headers[1].value = mime_types::extension_to_type(extension);

	cerr << "Req: " << req.uri << endl;
	cerr << "Response Content-Length: " << rep.content.size() << endl;
}

// Taken from boost_asio_server4/file_handler.cpp
bool general_handler::url_decode(const string& in, string& out)
{
	out.clear();
	out.reserve(in.size());
	for (size_t i = 0; i < in.size(); ++i)
	{
		if (in[i] == '%')
		{
			if (i + 3 <= in.size())
			{
				int value = 0;
				istringstream is(in.substr(i + 1, 2));
				if (is >> hex >> value)
				{
					out += static_cast<char>(value);
					i += 2;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		else if (in[i] == '+')
		{
			out += ' ';
		}
		else
		{
			out += in[i];
		}
	}
	return true;
}

general_handler::~general_handler()
{
}

} // namespace server4
} // namespace http
