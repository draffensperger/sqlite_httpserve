// Taken from: http://www.dreamincode.net/forums/topic/122300-sqlite-in-c/

#ifndef __SQLITE_DATABASE_H__
#define __SQLITE_DATABASE_H__

#include <string>
#include <vector>
#include "sqlite3/sqlite3.h"

using namespace std;

class result_set {
public:
    vector<string> fields;
    vector<vector<string> > data;
};

class sqlite_database
{
public:
	sqlite_database(const string& filename);
	~sqlite_database();

	bool open(const string& filename);
	result_set query(const string& query);
	void close();

private:
	sqlite3 *sqlite_db_;
};

#endif


