**Sqlite_httpserve** is an HTTP 1.0 server for static files and queries to a Sqlite3 database. The server sends static files from a directory, but files with a .sql extension are interpreted as queries to the sqlite database. The queries can have parameter placeholders in {curly braces} and may receive parameters via the URL query string. The results are returned in comma separated values (CSV) format. The code is based on the [Boost.Asio library's example server4](http://www.boost.org/doc/libs/1_55_0/doc/html/boost_asio/example/cpp03/http/server4/). 

# Usage

The initial use case for this server is to run locally on the client's computer to interact with a local sqlite database using a web interface.

To run the server, execute it as:

    ./sqlite_httpserve <network address> <port> <document root> <sqlite3 db file>

For instance, to listen on port 80 for all IPv4 addresses and serve non .sql static files from /var/wwwroot/ but executes .sql files in that directory as queries against the database /var/db.sqlite3, you would run: 

	./sqlite_httpserve 0.0.0.0 80 /var/wwwroot/ /var/db.sqlite3

Listening on IPv6 network addresses is also supported, e.g. using 0::0 in the example about instead of 0.0.0.0.

## Example query

If, for instance the sqlite database had a collection of accounts, there could be a file `/var/wwwroot/get_accounts.sql` which had the contents of `SELECT id, name FROM accounts WHERE id = {id};`. When the client would execute an HTTP request with that id, e.g. `http://server:port/get_accounts.sql?name=Bob's%20Account`, the query is run against the sqlite database with the properly decoded and escaped value, i.e. `'Bob''s Account'` substituted in for `{id}`.

The results would be returned in comma separated format with the first line being the field values, and carriage-return + newline (`"\r\n"`) separating the records. Fields that have newline or double quote characters are enclosed in double quotes (") and any double quotes in the value are represented as two double quotes (""). Example results could be:

	id,name\r\n
	124,Bob's Account\r\n
	135,"Account on two lines\r\n
	for Bob ""Rich Uncle"" Smith"\r\n

# Installation

This project embeds the code for [Sqlite 3.8.2](http://www.sqlite.org/download.html), but requires [Boost 1.55](http://www.boost.org/) or greater, must be downloaded and built separately. Instructions for specific systems are below.

There is also a [Code::Blocks](http://www.codeblocks.org) project file `sqlite_httpserve.cbp` and you can use that IDE to edit/build this server. The Code::Blocks project still uses `Makefile` for building and you would need the same requirements below.

## Linux

### Install Boost

On Ubuntu you can install pre-built Boost binaries with `apt-get`, but since this depends on the latest version of Boost (as of this writing), it's best to install Boost from source. 

Open a new terminal window and run the following (adapted from a [tutorial by Matthew Reid](http://particlephysicsandcode.wordpress.com/2013/03/11/installing-boost-1-52-ubuntu-12-04-fedora/)) 

	wget -O boost_1_55_0.tar.bz2 http://sourceforge.net/projects/boost/files/boost/1.55.0/boost_1_55_0.tar.bz2/download
	tar --bzip2 -xf boost_1_55_0.tar.bz2
	cd boost_1_55_0
	./bootstrap.sh
	sudo ./b2 install --with-system


### Run Make

Then, in the project directory, run `make` (the boost install will take care of putting the headers and libraries where GCC can access them).

## Windows

### Install MinGW

You will need to install [MinGW](http://www.mingw.org/), which provides the GCC compiler and some utilities as this project is not configured to use the Microsoft compiler (it may work but I haven't tried it).

### Install Boost

Download and Unzip the [Boost source files](http://sourceforge.net/projects/boost/files/boost/1.55.0/). I would recommend copying the extracted boost files to `C:\Program Files\boost\boost_1_55_0\`. If you put the boost files in another directory you will need to use that directory below and pass a `BOOST_ROOT` directory to `make`. 

These instructions are adapted from the Boost ["Getting Started" guide](http://www.boost.org/doc/libs/1_55_0/more/getting_started/windows.html):

Open a Command Prompt window, then run these commands to build Boost.

	cd "C:\Program Files\boost\boost_1_55_0"
	set PATH=%PATH%;C:\MinGW\bin;C:\MinGW\mingw32\bin;C:\MinGW\msys\1.0\bin
	bootstrap gcc
	b2 install --toolset=gcc --with-system

### Run Make

Go to `C:\Program Files\boost\boost_1_55_0\stage\lib` (or `stage\lib` within wherever you put your boost files), and note the suffix of the lib files. It should be something like `-mgw48-mt-d-1_55`.

Open a Command Prompt in the directory where you put the project files and run `make BOOST_LIB_SUFFIX=-mgw48-mt-d-1_55` (or whatever your boost lib suffix is). If you installed Boost in a directory besides `C:\Program Files\boost\boost_1_55_0\` then set add `BOOST_ROOT=[your boost directory]` to the make command. 

## Mac OS X

I don't have a Mac so I'm not sure exactly how to build this project on Mac OS X, but below are some suggestions of places to look. If you do build on Mac OS X, please let me know.

### Set up GCC on Mac OS X

Possible source: [How to use/install gcc on Mac OS X 10.8 / Xcode 4.4 (stackoverflow.com)](http://stackoverflow.com/questions/9353444/how-to-use-install-gcc-on-mac-os-x-10-8-xcode-4-4)

It would also be possible to edit the `Makefile` so that it would use the Xcode compiler instead of GCC.

### Get and install Boost

Possible source: [Boost on MacPorts.org](http://www.macports.org/ports.php?by=library&substr=boost)

Then build boost (commands are probably similar to the Linux installation above).

### Run Make

`make BOOST_LIB_SUFFIX=[your boost lib suffix] BOOST_ROOT=[your boost dir]`

# License and Acknowledgments

This project uses:

- [Sqlite](http://www.sqlite.org) which is public domain
- [Boost.Asio example server 4](http://www.boost.org/doc/libs/1_55_0/doc/html/boost_asio/example/cpp03/http/server4/), which is Copyright (c) 2003-2012 Christopher M. Kohlhoff (chris at kohlhoff dot com) and distributed under the Boost license (see below)
- Two helper classes, `sqlite_database.cpp` is from a [dreamincode.net forum post](http://www.dreamincode.net/forums/topic/122300-sqlite-in-c/) and `uricodec.cpp` is from a [CodeGuru.com article](http://www.codeguru.com/cpp/cpp/algorithms/strings/article.php/c12759/URI-Encoding-and-Decoding.htm). I would assume that usage of that code according to the Boost License terms is appropriate.
- The `Makefile`, this README and my modifications to `general_handler.cpp` to make it execute .sql files as sqlite queries and serve the results in CSV format are Copyright (c) 2013 David Raffensperger (contact @ davidraff dot com) and are also distributed under the Boost license. 


## Boost License

Boost Software License - Version 1.0 - August 17th, 2003

Permission is hereby granted, free of charge, to any person or organization
obtaining a copy of the software and accompanying documentation covered by
this license (the "Software") to use, reproduce, display, distribute,
execute, and transmit the Software, and to prepare derivative works of the
Software, and to permit third-parties to whom the Software is furnished to
do so, all subject to the following:

The copyright notices in the Software and this entire statement, including
the above license grant, this restriction and the following disclaimer,
must be included in all copies of the Software, in whole or in part, and
all derivative works of the Software, unless such copies or derivative
works are solely in the form of machine-executable object code generated by
a source language processor.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.